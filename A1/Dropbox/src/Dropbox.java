import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class Dropbox {
	private static String Key = "hrtsaslo6bfwu3r";
	private static String Secret = "o636tzykwxnp6il";
	
	public static void main(String[] args) throws TembooException, IOException {
		//Get the temboo session
		TembooSession session = new TembooSession("yz383", "myFirstApp", "758ff73af0404add8798a09ca551c9d1");

		//Initialize oAuth
		InitializeOAuthResultSet initilizeResult = initializeOAuth(session);
		String url = initilizeResult.get_AuthorizationURL();

		//Open the Authorization URL in the browser.
		openInBrowser(url);

		//Finalize the authorization
		FinalizeOAuthResultSet finalizeResult = finalizeOAuth(session, 
				initilizeResult.get_CallbackID(),initilizeResult.get_OAuthTokenSecret());

		//Download the __list file.
		String __list = new String(Base64.decodeBase64(get__list(session, 
				finalizeResult.get_AccessToken(),finalizeResult.get_AccessTokenSecret())));

		//Split the items along whitespace.
		String[] items = __list.split("\\s+");

		//Download and write each item.
		System.out.println("Copy file from Yue Zou's Dropbox.");
		String file="";
		for (int i=0; i<items.length; i+=2){
			file=getItem(session, items[i], finalizeResult.get_AccessToken(),
					finalizeResult.get_AccessTokenSecret());
			byte[] data = Base64.decodeBase64(file);
			//String word = fi
			System.out.println("Writting: "+items[i]);
			//System.out.println(word);
			try (BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream("/Users/Sakura/Documents/" +items[i+1]+"/"+items[i]))) {
				stream.write(data);			
			}
		}
		System.out.println("Finished!");
		
		//delete content in __list
		uploadFile(session,"__list", finalizeResult.get_AccessToken(),
				finalizeResult.get_AccessTokenSecret());
	}
	
	//Downloads the "move/__list file"
	private static String get__list(TembooSession session, String accessToken, 
			String accesstokenSecret) throws TembooException{
		return getFile(session, "/move/__list", accessToken, accesstokenSecret, false);
	}
	
	//Downloads any file from Dropbox.
	private static String getItem(TembooSession session, String location, String accessToken, 
			String accesstokenSecret) throws TembooException{
		return getFile(session, location, accessToken, accesstokenSecret, true);
	}
	
	//Gets a file from Dropbox.
	private static String getFile(TembooSession session, String path, String accessToken, 
			String accesstokenSecret, Boolean base64) throws TembooException{
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AccessToken(accessToken);
		getFileInputs.set_AppSecret(Secret);
		getFileInputs.set_Root("auto");
		getFileInputs.set_AccessTokenSecret(accesstokenSecret);
		getFileInputs.set_AppKey(Key);
		getFileInputs.set_Path(path);
		//getFileInputs.set_EncodeFileContent(base64);

		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		return getFileResults.get_Response();
	}
	
	//Upload File
	private static String uploadFile(TembooSession session, String fileName, String accessToken, 
			String accesstokenSecret) throws TembooException{
		UploadFile uploadFileChoreo = new UploadFile(session);
		
		// Get an InputSet object for the choreo
		UploadFileInputSet uploadFileInputs = uploadFileChoreo.newInputSet();
		
		// Set inputs
		uploadFileInputs.set_Folder("/move/");
		uploadFileInputs.set_AppSecret(Secret);
		uploadFileInputs.set_AccessToken(accessToken);
		uploadFileInputs.set_FileName(fileName);
		uploadFileInputs.set_AccessTokenSecret(accesstokenSecret);
		uploadFileInputs.set_AppKey(Key);
		uploadFileInputs.set_FileContents("a");
		
		// Execute Choreo
		UploadFileResultSet uploadFileResults = uploadFileChoreo.execute(uploadFileInputs);
		
		System.out.println("__list has been updated!");
		
		return "File upload success: " + uploadFileResults.get_Response();
	}

	//Initialize OAuth with Temboo
	private static InitializeOAuthResultSet initializeOAuth(TembooSession session) throws TembooException {
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppSecret(Secret);
		initializeOAuthInputs.set_DropboxAppKey(Key);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = 
				initializeOAuthChoreo.execute(initializeOAuthInputs);

		return initializeOAuthResults;
	}
	
	//Finalize OAuth with Temboo
	private static FinalizeOAuthResultSet finalizeOAuth(TembooSession session, String callback, 
			String tokenSecret) throws TembooException{
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callback);
		finalizeOAuthInputs.set_DropboxAppKey(Key);
		finalizeOAuthInputs.set_DropboxAppSecret(Secret);
		finalizeOAuthInputs.set_OAuthTokenSecret(tokenSecret);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = 
				finalizeOAuthChoreo.execute(finalizeOAuthInputs);

		return finalizeOAuthResults;
	}
	
	//Open default browser
	private static void openInBrowser(String url){
		try {
			java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
		}catch (java.io.IOException e) {
			System.out.println(e.getMessage());
		}catch(java.awt.HeadlessException e){
			System.out.println("Please go to the following url in your browser:" + url);
		}
	}
	
}