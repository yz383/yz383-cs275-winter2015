Yue Zou (yz383)
Web and Mobile App Development
Assignment 1
email: yz383@drexel.edu

The included source code in Dropbox.java is to to use Temboo to perform OAuth between Dropbox and the application. The main program searches my Dropbox entries for files in a directory called "move."  A file within this directory called "__list" will specify how to move files. 

he code for Dropbox.java were written with Eclipse on a OS X Yosemite and tested and debugged with Eclipse. 

To run Dropbox.java you must load the code into Eclipse then simply run the program. The program will automatically open your default browser to authorize. Then it will copy file from my dropbox folder to own filesystem.
The program will run with the default client key and client secret.

I tested it with different content in __list file.

From this assignment, I get more familiar with authorize app using Temboo. I learn how to upload/download file from dropbox account.