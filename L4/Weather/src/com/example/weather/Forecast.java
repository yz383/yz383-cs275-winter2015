package com.example.weather;

import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

public class Forecast {

	public String date;
	public String temp;
	public String humidity;
	public String condition;
	public String icon;

	public Forecast(JsonObject object){
		try{
			this.date = object.getAsJsonObject().get("pretty").getAsString();
			this.temp = object.get("temp").getAsJsonObject().get("english").getAsString() + "°";
			this.humidity = object.get("humidity").getAsString() + "%";
			this.condition = object.get("condition").getAsString();
			this.icon = object.get("icon_url").getAsString();
		}catch (JsonIOException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Forecast> fromJson(JsonArray forecasts2){
		ArrayList<Forecast> forecasts = new ArrayList<Forecast>();
		for (int i = 0; i < 24; i++) {
			try {
				forecasts.add(new Forecast(forecasts2.get(i).getAsJsonObject()));
			}catch (JsonIOException e){
				e.printStackTrace();
			}
		}
		
		return forecasts;
		
	}
}
