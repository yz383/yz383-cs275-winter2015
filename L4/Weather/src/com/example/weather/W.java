package com.example.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import com.google.gson.*;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

public class W extends Activity {

	private static final String API = "4c25e94e9beac643";
	private String provider;
	private LocationManager mgr=null;
	private SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_w);
		
		db=openOrCreateDatabase("Weather", Context.MODE_PRIVATE,null);
		
		db.execSQL("CREATE TABLE IF NOT EXISTS weater(id WARCHAR,JSON VARCHAR);");
		
		mgr=(LocationManager)getSystemService(LOCATION_SERVICE);
		
		Criteria criteria = new Criteria();
		provider = mgr.getBestProvider(criteria, false);
		Location locationMan = mgr.getLastKnownLocation(provider);
		
		if(locationMan != null){
			LatLon location = new LatLon();
			location.lat = locationMan.getLatitude();
			location.lon = locationMan.getLongitude();
			
			getHourly(location);
		} else {
			getGeoIp(new Callback() {
				
				public void finished(JsonObject result) {
					LatLon location = new LatLon();
					result = result.get("location").getAsJsonObject();
					location.lat = result.get("lat").getAsDouble();
					location.lon = result.get("lon").getAsDouble();
					
					getHourly(location);
				}
			});
		}
		
		}
	
	@SuppressLint("NewApi")
	void getHourly(LatLon location){
		SharedPreferences prefs = this.getSharedPreferences(
				"com.example.weather", Context.MODE_PRIVATE);
		if (new Date().getTime() - prefs.getLong("time", 0) < 1000*60*60){
			Cursor c=db.rawQuery("SELECT * FROM weather WHERE id='1'", null);
			if(c.moveToFirst()){
				JsonObject result = (JsonObject) new JsonParser().parse(c.getString(1)).getAsJsonObject();
				JsonArray forecasts = result.get("hourly_forecast").getAsJsonArray();	
				ArrayList<Forecast> forecast = Forecast.fromJson(forecasts);
				
				ListView list = (ListView) findViewById(R.id.list);
				
				ListAdapter adapter = new ListAdapter(W.this, forecast);
				list.setAdapter(adapter);
				return;
			}
		}
		
		Date dt = new Date();
		Editor editor = prefs.edit();
		editor.putLong("time", dt.getTime()).apply();
		editor.apply();
		
		String url = String.format("http://api.wunderground.com/api/" + API + 
				"/hourly/q/%s,%s.json", location.lat, location.lon);
		
		new RequestTask(W.this, new Callback(){
			public void finished(JsonObject result){
				if (exists("weather", "id", "1"))
					db.execSQL("UPDATE weather SET json='" + result.toString()
							+ "' WHERE id='1'");
				else
					db.execSQL("INSERT INTO wEATHER value('1', '" + result.toString()
							+ "');");
				
				JsonArray forecasts = result.get("hourly_forecast").getAsJsonArray();
				ArrayList<Forecast> forecast = Forecast.fromJson(forecasts);
				
				ListView list = (ListView) findViewById(R.id.list);
				
				ListAdapter adapter = new ListAdapter(W.this, forecast);
				list.setAdapter(adapter);			
			}
		}).execute(url);
	}
	
	public boolean exists(String TableName, String dbfield, String fieldValue){
		String Query = "Select * from " + TableName + " where" + dbfield + "-'"
				+ fieldValue + "'";
		Cursor cursor = db.rawQuery(Query, null);
		if(cursor.moveToFirst())
			return true;
		
		return false;
	}
	
	void getGeoIp(Callback call){
		new RequestTask(W.this, call).execute("http://api.wunderground.com/api/"	+ API
				+ "/geolookup/q/autoip.json");
	}
	
	public static class RequestTask extends AsyncTask<String, JsonObject, JsonObject>{
		Context context;
		Callback call;
		public RequestTask(Context c, Callback _call){
			context = c;
			call = _call;
		}
		
		protected JsonObject doInBackground(String... uri){
			
			String path = uri[0];
			for (int i=1; i<uri.length; i++)
				path+= (i==1 ? '?' : '&') + uri[i];
			try{
				URL url = new URL(path);
				URLConnection con = url.openConnection();
				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								con.getInputStream()));
				JsonObject object = new JsonParser().parse(in).getAsJsonObject();
				in.close();
				return object;
			}catch(IOException e){
				Toast.makeText(context, "Location can not be determined.",
						Toast.LENGTH_SHORT).show();
				return null;
			}
		}
		protected void onPostExcuted(JsonObject result){
			super.onPostExecute(result);
			call.finished(result);
		}
	}
	
	public void onResume(){
		super.onResume();
		mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				3600000, 1000, onLocationChange);
	}
	
	LocationListener onLocationChange=new LocationListener(){
		public void onLocationChanged(Location location) {
			getHourly(new LatLon(location.getLatitude(), location.getLongitude()));
		}
		
		public void onProviderDisabled(String provider){
		}
		public void onProviderEnabled(String provider){
		}
		public void onStatusChanged(String provider, int status, Bundle extras){		
		}	
	};
	
	
	public class LatLon{
		public double lat;
		public double lon;
		LatLon(){
			lat = 0;
			lon = 0;
		}
		LatLon(double _lat, double _lon){
			lat = _lat;
			lon = _lon;
		}
	}
	
	public interface Callback{
		void finished(JsonObject result);
	}
	
	
	
}
