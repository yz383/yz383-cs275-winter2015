package com.example.weather;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<Forecast> {
	public ListAdapter(Context context, ArrayList<Forecast> forecasts){
		super(context, 0, forecasts);
	}
	
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Forecast forecast = getItem(position);    
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
           convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_forecast, parent, false);
        }
        // Lookup view for data population
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView temp = (TextView) convertView.findViewById(R.id.temp);
        TextView humidity = (TextView) convertView.findViewById(R.id.humidity);
        TextView condition = (TextView) convertView.findViewById(R.id.condition);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        // Populate the data into the template view using the data object
        date.setText(forecast.date);
        temp.setText(forecast.temp);
        humidity.setText(forecast.humidity);
        condition.setText(forecast.condition);
        icon.setImageDrawable(LoadImageFromWebOperations(forecast.icon));
        // Return the completed view to render on screen
        return convertView;
    }

	private Drawable LoadImageFromWebOperations(String url) {
		try
        {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        }catch (Exception e) {
            System.out.println("Exc="+e);
            return null;
        }
	}	
	
}
