Yue Zou (yz383)
Web and Mobile App Development
Lab4
email: yz383@drexel.edu

The included source code in W.java is an Android application that pulls data from a web service, and displays it on a layout. 

he code for W.java were written with Eclipse on a OS X Yosemite and tested and debugged with Eclipse. 

To run W.java you must load the code into Eclipse then simply run the program with android virtual device. The program will show the hourly weather.

From this assignment, I learn how to create an Android application using GPS and get data online with Eclipse.