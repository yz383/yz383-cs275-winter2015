import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonElement;
// Requires gson jars
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Wunderground {         
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// Get from http://www.wunderground.com/weather/api/
		String key;
		System.out.println("Enter key: ");
		Scanner in = new Scanner(System.in);
		key = in.nextLine();
		if(key.length() == 0) {
			System.out.println("No input. Run with defult key.");
			key = "4c25e94e9beac643";
		} 
		
		String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// Convert to a JSON object to print data
    	JsonParser jp = new JsonParser();
    	JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    	JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
    	
    	// Get some data elements and print them
    	int zip = rootobj.get("location").getAsJsonObject().get("zip").getAsInt();
    	System.out.println("zip code: " + zip);
    	
    	String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
    	System.out.println("city: " + city);
    	
    	String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
    	System.out.println("state: " + state);
    	
    	Double lat = rootobj.get("location").getAsJsonObject().get("lat").getAsDouble();
    	System.out.println("lat: " + lat);
    	
    	Double lon = rootobj.get("location").getAsJsonObject().get("lon").getAsDouble();
    	System.out.println("long: " + lon);
    	
    	
    	String sURL_1 = "http://api.wunderground.com/api/" + key + "/hourly/q/" + state + "/" + city + ".json";
		
		// Connect to the URL
		URL url_1 = new URL(sURL_1);
		HttpURLConnection request_1 = (HttpURLConnection) url_1.openConnection();
		request_1.connect();
		
		// Convert to a JSON object to print data
    	JsonParser jp_1 = new JsonParser();
    	JsonElement root_1 = jp_1.parse(new InputStreamReader((InputStream) request_1.getContent()));
    	JsonObject rootobj_1 = root_1.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
    	
    	// Get some data elements and print them
    	for(int i = 0; i < 24; i = i+1) {
    	String hour = rootobj_1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
    	System.out.println(hour);
    	
    	String condition = rootobj_1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("condition").getAsString(); 
    	System.out.println("condition: " + condition);
    	
    	Double temp = rootobj_1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsDouble();
    	System.out.println("temperature: " + temp + "°F");
    	
    	int humidity = rootobj_1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("humidity").getAsInt();
    	System.out.println("humidity: " + humidity + "%");
    	}
	}

}