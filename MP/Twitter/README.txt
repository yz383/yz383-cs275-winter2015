Yue Zou (yz383)
Web and Mobile App Development
Midterm Practicum
email: yz383@drexel.edu

The included source code in Twitter.java is to to use Temboo to perform OAuth between Twitter and the application. The main program will prompt user to enter a twitter user name(run with default with no input). Then it lists 30 tweets. The program use Wordnik to look for syllable word in 30 tweets then print the polysyllable word and the count, calculate the SMOG grade of the twitter user.

he code for Twitter.java were written with Eclipse on a OS X Yosemite and tested and debugged with Eclipse. 

To run Twitter you must load the code into Eclipse then simply run the program. The program will automatically open your default browser to authorize. You will be prompted to enter a twitter user name(run with default with no input). Then it will list all tweets and print the polysyllable word and the SMOG grade. 
The program will run with the default client key and client secret.

I tested it in different twitter user id and got good result if there is no emoji tweets.

From this practicum, I get more familiar with authorize app and connect json by java.