import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class Twitter {
	private static String Key = "GeMoJ5Sz2pPuSQHUrv9zhNeBq";
	private static String Secret = "9IQWdHBBvrLI429ubEYyrSAJeWBggcm66xq8lp0cmuoUX0PJco";
	private static String WordnikKey = "1185adbe7cf30b6979000087639058c6ceedd04655fc1c1bd";
	
	public static void main(String[] args) throws Exception {
		//Temboo session
		TembooSession session = new TembooSession("yz383", "myFirstApp", "758ff73af0404add8798a09ca551c9d1");
		
		//Initialize OAuth
		InitializeOAuthResultSet initilizeResult = initializeOAuth(session);
		String url = initilizeResult.get_AuthorizationURL();
		
		//Open default browser to authorize app
		openInBrowser(url);
		
		//Finalize OAuth
		FinalizeOAuthResultSet finalizeResult = finalizeOAuth(session,initilizeResult.get_CallbackID(),
				initilizeResult.get_OAuthTokenSecret());
		
		//Prompt Twitter user name
		String user;
		System.out.println("Enter the name of a Twitter user:(No input run with default:TheEllenShow)");
		Scanner in = new Scanner(System.in);
		user = in.nextLine();
		if (user.length() == 0){
			user = "TheEllenShow"; //default user
		}
		
		//Get 30 tweet and print
		JsonArray root = getTimeline(user,session,finalizeResult.get_AccessToken(),finalizeResult.get_AccessTokenSecret());

		for (int i=0; i<root.size(); i++){
			String tweet = root.get(i).getAsJsonObject().get("text").getAsString();
			System.out.println(tweet);
		}
		
		int polySyllableCount = 0;
		
		//Get polysyllabic word, print and count, calculate the SMOG
		System.out.println("This user's polysyllabic word:");
		for (int i=0; i<root.size(); i++){
			String tweet = root.get(i).getAsJsonObject().get("text").getAsString();
			String[] words=tweet.split(" ");
			for (int j=0; j<words.length; j++){
				String word = words[j].replaceAll("(?i)[^a-zA-Z0-9\u4E00-\u9FA5]", "");	
				//System.out.println(word);
				int sylables = syllable(word);				
				if (sylables>=3){
					System.out.println(word);
					polySyllableCount++;
				}		
			}
		}
		
		double score = 1.0430*Math.sqrt(polySyllableCount * (30 / root.size())) + 3.1291;
		System.out.println("Count of polysyllabic word:" + polySyllableCount);
		System.out.println("SMOG Score:" + score);
	}
	
	//Use wordnik to define syllable word
	private static int syllable (String word) throws Exception{

		String sURL = "http://api.wordnik.com:80/v4/word.json/" + word + 
				"/hyphenation?useCanonical=false&limit=50&api_key=" + WordnikKey;
		
		if (sURL.contains("json//")){
			int syllable = 0;
			return syllable;
		}
		
		//Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		//Convert to JsonArray
		JsonParser jp = new JsonParser();
    	JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    	String temp = root.toString();
    	if (temp=="[]"){	//empty array
    		int syllable = 0;
    		return syllable;
    	}
    	else {
    		JsonArray rootArray = root.getAsJsonArray();
    		int syllable = rootArray.size();
    		return syllable;
    	}    	
	}
	
	//Get Twitter Timeline with Temboo
	private static JsonArray getTimeline(String user, TembooSession session,
			String accessToken, String tokenSecret) throws TembooException {
		
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(user);
		userTimelineInputs.set_AccessToken(accessToken);
		userTimelineInputs.set_AccessTokenSecret(tokenSecret);
		userTimelineInputs.set_ConsumerSecret(Secret);
		userTimelineInputs.set_ConsumerKey(Key);
		userTimelineInputs.set_Count(new Integer(30));
		
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		String result = userTimelineResults.get_Response();
		JsonParser p = new JsonParser();
		
		JsonArray root = p.parse(result).getAsJsonArray();
		
		return root;
	}

	//Initialize OAuth with Temboo
	private static InitializeOAuthResultSet initializeOAuth(TembooSession session) throws TembooException {
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
		
		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ConsumerSecret(Secret);
		initializeOAuthInputs.set_ConsumerKey(Key);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		return initializeOAuthResults;	
	}
	
	//Finalize OAuth with Temboo
	private static FinalizeOAuthResultSet finalizeOAuth(TembooSession session, 
			String callback, String tokenSecret) throws TembooException {
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callback);
		finalizeOAuthInputs.set_OAuthTokenSecret(tokenSecret);
		finalizeOAuthInputs.set_ConsumerSecret(Secret);
		finalizeOAuthInputs.set_ConsumerKey(Key);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		return finalizeOAuthResults;	
	}
	
	//Open default browser
	private static void openInBrowser(String url){
		try {
			java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
		}catch(java.io.IOException e) {
			System.out.println(e.getMessage());
		}catch(java.awt.HeadlessException e){
			System.out.println("Please go to the following url in your browser:" + url);
		}
		
	}

}