Yue Zou (yz383)
Web and Mobile App Development
Lab3
email: yz383@drexel.edu

The included source code in TicTacToe.java is an Android application with a layout and listener. It’s a tic-tac-toe game. Two players will take turns clicking on one of the 9 buttons, which will turn it into an X or an O, depending on the player turn. Once a button has been clicked, it will have no effect if clicked again. A player wins if there is a row, column, or diagonal, filled with three consecutive X's or three consecutive O's. If all 9 buttons are clicked and there is no winner, the game is declared a tie.

he code for TicTacToe.java were written with Eclipse on a OS X Yosemite and tested and debugged with Eclipse. 

To run TicTacToe.java you must load the code into Eclipse then simply run the program with android virtual device. The program will prompt you the click the button to play the tic-tac-toe game.

I tested it in different win condition such as O win, X win or tie.

From this assignment, I learn how to create an Android application with Eclipse.