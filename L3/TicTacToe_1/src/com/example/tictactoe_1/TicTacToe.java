package com.example.tictactoe_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TicTacToe extends Activity {
	private Board board;
	private int clickCount;
	private static int ties=0, xWin=0, oWin=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tic_tac_toe);
		
		//Initialize scores
		((TextView) findViewById(R.id.ties)).setText("Ties:" + ties);
		((TextView) findViewById(R.id.xWin)).setText("X:" + xWin);
		((TextView) findViewById(R.id.oWin)).setText("O:" + oWin);
		
		ImageView[][] views = new ImageView[3][3];
		final String baseName = "imageView";
		
		int j=1;
		for (int i=1; i<=9; i++){
			int resID = TicTacToe.this.getResources().getIdentifier
					(baseName+i, "id", TicTacToe.this.getPackageName());
			ImageView b = (ImageView) findViewById(resID);
			views[(i-3)%3][j-1]=b;
			b.setOnClickListener(new Click((i-1)%3, j-1, resID));
			if (i%3==0) j++;
		}
		
		board = new Board(views);
	}

	private class Click implements OnClickListener {
		private int i, j, resId;
		
		public Click(int _i, int _j, int _resId){
			resId = _resId;
			i = _i;
			j = _j;
		}

		@Override
		public void onClick(View v) {
			clickCount++;
			
			if (board.insert(i, j, resId)){
				if (board.user1)
					xWin++;
				else
					oWin++;
				Toast.makeText(TicTacToe.this, (board.user1?"X":"0") + "Wins!",
						Toast.LENGTH_SHORT).show();
				Intent intent = getIntent();
				finish();
				startActivity(intent);
				return;
			}
			
			if (clickCount>=9){
				ties++;
				
				Toast.makeText(TicTacToe.this, "It's a tie!", 
						Toast.LENGTH_SHORT).show();
				Intent intent = getIntent();
				finish();
				startActivity(intent);
				return;
			}
		}
	}
	
	private class Board{
		private int[][] board = new int[3][3];
		
		private ImageView[][] views;
		
		public Boolean user1 = true;
		
		public Board(ImageView[][] _views){
			views = _views;
		}
		
		public Boolean insert(int i, int j, int resId){
			int user = user1?1:2;
			
			if (board[i][j]==0){
				board[i][j]=user;
				ImageView button = (ImageView) findViewById(resId);
				if (user1){
					button.setImageResource(R.drawable.o);
				}else{
					button.setImageResource(R.drawable.x);
				}
				user1=!user1;
			}
			if (checkDown(i,user)) return true;
			if (checkRight(j,user)) return true;
			if (checkDiagonals(user)) return true;
			
			return false;
		}
		
		private Boolean checkDown(int row, int user){
			for (int j=0; j<3; j++){
				if (board[row][j]!=user)
					return false;
			}
			return true;
		}
		
		private Boolean checkRight(int column, int user){
			for (int i=0; i<3; i++){
				if (board[i][column]!=user)
					return false;
			}
			return true;
		}
		
		private Boolean checkDiagonals(int user){
			Boolean strike1=false;
			if (board[1][1]!=user) return false;
			if (board[0][0]!=user) strike1=true;
			if (board[2][2]!=user) strike1=true;
			if (!strike1) return true;
			if (board[2][0]!=user) return false;
			if (board[0][2]!=user) return false;
			return true;
		}
	}
}
