Yue Zou (yz383)
Web and Mobile App Development
Lab 2
email: yz383@drexel.edu

The included source code in Google.java is to use the Google web service API to invoke the Google Calendar service interface. Perform OAuth authentication against the account and obtain a token. Use that token to list all of the calendars in Google Calendar. For the first calendar that you find, print all the events within that calendar. The main program will prompt the user to enter the client id, client secret and redirect url and ’code’ url parameter you get. 
The included code in Temboo.java is to use Temboo to perform OAuth between Google and the application. The program will list all calendars and, for the first one you find, list all the events within that calendar. 
Both of them will run with the default client id and client secret.

The code for Google.java and Temboo.java were written with Eclipse on a OS X Yosemite and tested and debugged with Eclipse. 

To run Google.java you must load the code into Eclipse then simply run the program. You will be promoted you enter the client id, client secret and redirect url. Then it will ask you to go to one address to get the code, prompt you to enter the code. Then it will automatically show all calendars and the events in first calendar.
To run Temboo.java you must load the code into Eclipse then simply run the program. You will automatically show all calendars and the events in first calendar 
Both of them will run with the default client id and client secret if there id no input.

I tested it in client id and client secret.

From this lab, I learn how to invoke web service with authentication needed.