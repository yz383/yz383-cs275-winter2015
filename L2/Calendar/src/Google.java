import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.gson.JsonObject;
import com.sun.org.glassfish.external.statistics.Statistic;

import java.io.IOException;
import java.util.Collections;
import java.util.Scanner;
import java.util.Set;

public class Google {

    public static void main(String[] args) throws IOException{
    	
    	Set<String> scope = Collections.singleton(CalendarScopes.CALENDAR);
    	
    	//prompt input
    	String ClientID;
		System.out.println("Enter Client ID: ");
		Scanner in = new Scanner(System.in);
		ClientID = in.nextLine();
		if(ClientID.length() == 0) {
			System.out.println("No input. Run with defult Client ID.");
			ClientID = "504912881781-c5up8np03o7bd6rqfh6eb7ks5cj24eei.apps.googleusercontent.com";
		}
		String ClientSecret;
		System.out.println("Enter Client Secret: ");
		Scanner in1 = new Scanner(System.in);
		ClientSecret = in1.nextLine();
		if(ClientSecret.length() == 0) {
			System.out.println("No input. Run with defult Client Secret.");
			ClientSecret = "FxhWHVMYlifibOKkS1SLCxCJ";
		}
		String redirectUrl = "";
		System.out.println("Enter redirectUrl: ");
		Scanner in2 = new Scanner(System.in);
		redirectUrl = in2.nextLine();
		if(redirectUrl.length() == 0) {
			System.out.println("No input. Run with defult url.");
			redirectUrl = "https://www.cs.drexel.edu/";
		}
    	
    	HttpTransport httpTransport = new NetHttpTransport(); 
    	JsonFactory jsonFactory = new JacksonFactory();
    	GoogleAuthorizationCodeFlow.Builder codeFlowBuilder = 
                new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, 
                        jsonFactory, 
                        ClientID, 
                        ClientSecret, 
                        scope
                );
    	GoogleAuthorizationCodeFlow codeFlow = codeFlowBuilder.build();
    	
        //set the code flow to use a dummy user
        String userId = "yz383";
        
        //"redirect" to the authentication url
        String redirectUri = redirectUrl;
        GoogleAuthorizationCodeRequestUrl authorizationUrl = codeFlow.newAuthorizationUrl();
        authorizationUrl.setRedirectUri(redirectUri);
        System.out.println("Go to the following address:");
        System.out.println(authorizationUrl);
        
        //use the code that is returned as a url parameter
        //to request an authorization token
        System.out.println("What is the 'code' url parameter?");
        String code = new Scanner(System.in).nextLine();
        GoogleAuthorizationCodeTokenRequest tokenRequest = codeFlow.newTokenRequest(code);
        tokenRequest.setRedirectUri(redirectUri);
        GoogleTokenResponse tokenResponse = tokenRequest.execute();

        Credential credential = codeFlow.createAndStoreCredential(tokenResponse, userId);
        
        //Credentials may be used to initialize http requests
        HttpRequestInitializer initializer = credential;
        //and thus are used to initialize the calendar service
        Calendar.Builder serviceBuilder = new Calendar.Builder(
                httpTransport, jsonFactory, initializer);
        Calendar calendar = serviceBuilder.build();
        
        //get some data
        System.out.println("All calender:");
        Calendar.CalendarList.List listRequest = calendar.calendarList().list();
        CalendarList feed = listRequest.execute();
        for(CalendarListEntry entry:feed.getItems()){
            System.out.println("ID: " + entry.getId());
            System.out.println("Summary: " + entry.getSummary());
        }
        
        System.out.println("");
        String id = feed.getItems().get(0).getId();
        Calendar.Events.List listEvent = calendar.events().list(id);
        Events events = listEvent.execute();
        System.out.println("All event in the first calendar:");
        for(Event event : events.getItems()) {
        	String dateTime = event.getEnd().getDateTime().toString();
        	String[] Time = dateTime.split("T");
        	String date = Time[0];
        	String time = Time[1];
        	System.out.println("Event Date: " + date);
        	System.out.println("Event Time: " + time);
        	System.out.println("Event: " + event.getSummary());
        } 		
	
        
    }
}
