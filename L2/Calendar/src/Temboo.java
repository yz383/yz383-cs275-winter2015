
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import com.google.gson.*;

public class Temboo {
	private static String ClientID = "504912881781-goqlpueqdhgc1v63rev821ta6r5must4.apps.googleusercontent.com";
	private static String ClientSecret = "wfpYdvs5shrz2F6ONCPLFpjq";
	private static String RefreshToken = "1/IfWYc4hIk8kWGDcSsYrLwRVyMboW-R9Yr9KRvKJkbCUMEudVrK5jSpoR30zcRFq6";
	
	public static void main(String[] args) throws TembooException {
		
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("yz383", "myFirstApp", "758ff73af0404add8798a09ca551c9d1");

		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret(ClientSecret);
		getAllCalendarsInputs.set_RefreshToken(RefreshToken);
		getAllCalendarsInputs.set_ClientID(ClientID);

		// Execute Choreo
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
		
        //System.out.println(getAllCalendarsResults.get_Response());
        
		System.out.println("All calender:");
        // Now parse the json
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(getAllCalendarsResults.get_Response());
        JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

        JsonArray items = rootobj.get("items").getAsJsonArray();
        for(int i = 0; i < items.size(); i++) {
        	JsonObject item = items.get(i).getAsJsonObject();
        	String id = item.get("id").getAsString();
        	String summary = item.get("summary").getAsString();
        	System.out.println("Calendar id: " + id);
        	System.out.println("Calendar: " + summary);
        }        
        
        System.out.println("");
        String id = items.get(0).getAsJsonObject().get("id").getAsString();
        System.out.println("All event in the first calendar:");
        GetAllEvent(id);
	}
	
	public static void GetAllEvent(String id) throws TembooException{
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("yz383", "myFirstApp", "758ff73af0404add8798a09ca551c9d1");

		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set inputs
		getAllEventsInputs.set_ClientSecret(ClientSecret);
		getAllEventsInputs.set_CalendarID(id);
		getAllEventsInputs.set_RefreshToken(RefreshToken);
		getAllEventsInputs.set_ClientID(ClientID);

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
		
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(getAllEventsResults.get_Response());
        JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

        JsonArray items = rootobj.get("items").getAsJsonArray();
        for(int i = 0; i < items.size(); i++) {
        	JsonObject item = items.get(i).getAsJsonObject();
        	String dateTime = item.get("end").getAsJsonObject().get("dateTime").getAsString();
        	String summary = item.get("summary").getAsString();
        	String[] Time = dateTime.split("T");
        	String date = Time[0];
        	String time = Time[1];
        	System.out.println("Event Date: " + date);
        	System.out.println("Event Time: " + time);
        	System.out.println("Event: " + summary);
        } 		
		
	}
}

